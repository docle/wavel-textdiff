<?php
    namespace Wavel\Textdiff;

    use Wavel\Textdiff\FineDiff;
    /**
     * Editor: Sourav@wavel.ai
     */


    class DiffService
    {
        public $cache_lo_water_mark = 900;
        public $cache_hi_water_mark = 1100;
        public $compressed_serialized_filename_extension = '.store.gz';

        public int $granularity = 2;
        public string $from_text = '';
        public string $to_text = '';
        public string $diff_opcodes = '';
        public int $diff_opcodes_len = 0;
        public string $data_key = '';
        public $start_time;

        public $punctuations = ['.', ',', ':', ';', '?', '!', '(', ')'];

        public function __construct()
        {
            $this->start_time = gettimeofday(true);
        }

        public function stripslashes_deep(&$value)
        {
            $value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
            return $value;
        }

        public function getDiff($from, $to, $granularity): ?array
        {
            if (empty($from) || empty($to) || empty($granularity) ) {
                return NULL;
            }

            $totalWordsInParagraph = str_word_count($from);

            $from = $this->stripslashes_deep($from);
            $to = $this->stripslashes_deep($to);

            if ( ctype_digit($granularity) ) {
                $granularity = max(min(intval($granularity), 3), 0);
            }
            // limit input
            $from_text = substr($from, 0, 1024*100);
            $to_text = substr($to, 0, 1024*100);

            $fineDiff = new FineDiff();

            $granularityStacks = [
                FineDiff::$paragraphGranularity,
                FineDiff::$sentenceGranularity,
                FineDiff::$wordGranularity,
                FineDiff::$characterGranularity
            ];

            $diff_opcodes = $fineDiff->getDiffOpcodes($from_text, $to_text, $granularityStacks[$granularity]);
            $diff_opcodes_len = strlen($diff_opcodes);
            $exec_time = gettimeofday(true) - $this->start_time;
            if ($diff_opcodes_len) {
                $data_key = sha1(serialize(array('granularity' => $granularity, 'from_text' => $from_text, 'diff_opcodes' => $diff_opcodes)));
                $filename = "{$data_key}{$this->compressed_serialized_filename_extension}";
                if (!file_exists("./cache/{$filename}")) {
                    // purge cache if too many files
                    if (!(time() % 100)) {
                        $files = glob("./cache/*{$this->compressed_serialized_filename_extension}");
                        $num_files = $files ? count($files) : 0;
                        if (
                            $num_files > $this->cache_hi_water_mark
                        ) {
                            $sorted_files = array();
                            foreach ($files as $file) {
                                $sorted_files[strval(@filemtime("./cache/{$file}")) . $file] = $file;
                            }
                            ksort($sorted_files);
                            foreach ($sorted_files as $file) {
                                @unlink("./cache/{$file}");
                                $num_files -= 1;
                                if ($num_files < $this->cache_lo_water_mark) {
                                    break;
                                }
                            }
                        }
                    }
                    // save diff in cache
                    $data_to_serialize = array(
                        'granularity' => $granularity,
                        'from_text' => $from_text,
                        'diff_opcodes' => $diff_opcodes,
                        'data_key' => $data_key,
                    );
                    $serialized_data = serialize($data_to_serialize);
                    @file_put_contents("./cache/{$filename}", gzcompress($serialized_data));
                    @chmod("./cache/{$filename}", 0666);
                }
            }
            $rendered_diff = $fineDiff->renderDiffToHTMLFromOpcodes($from_text, $diff_opcodes);
            $totalChanges = FineDiff::$changeCountArray;
            $totalChangeCOunt = count($totalChanges);
            $changePercentage = round(($totalChangeCOunt/$totalWordsInParagraph) * 100, 2);
//         echo '<pre>';
//         print_r($totalChanges);
            return ['changes' => $rendered_diff, 'totalChanges' => $totalChangeCOunt, 'percentage' => $changePercentage];
        }
    }


