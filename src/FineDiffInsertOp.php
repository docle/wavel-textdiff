<?php

    namespace Wavel\Textdiff;

    /**
     * Usage (simplest):
     *
     *   include 'finediff.php';
     *
     *   // for the stock stack, granularity values are:
     *   // FineDiff::$paragraphGranularity = paragraph/line level
     *   // FineDiff::$sentenceGranularity = sentence level
     *   // FineDiff::$wordGranularity = word level
     *   // FineDiff::$characterGranularity = character level [default]
     *
     *   $opcodes = FineDiff::getDiffOpcodes($from_text, $to_text [, $granularityStack = null] );
     *   // store opcodes for later use...
     *
     *   ...
     *
     *   // restore $to_text from $from_text + $opcodes
     *   include 'finediff.php';
     *   $to_text = FineDiff::renderToTextFromOpcodes($from_text, $opcodes);
     *
     *   ...
     */
    class FineDiffInsertOp extends FineDiffOp
    {
        public function __construct($text)
        {
            $this->text = $text;
        }

        public function getFromLen(): int
        {
            return 0;
        }

        public function getToLen(): int
        {
            return strlen($this->text);
        }

        public function getText()
        {
            return $this->text;
        }

        public function getOpcode(): string
        {
            $to_len = strlen($this->text);
            if ($to_len === 1) {
                return "i:{$this->text}";
            }
            return "i{$to_len}:{$this->text}";
        }
    }